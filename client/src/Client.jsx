import React, { Component } from 'react';
import ApolloClient, { createNetworkInterface } from 'apollo-client';
import { ApolloProvider } from 'react-apollo';
import Routes from './Routes';

// Network Interface
const networkInterface = createNetworkInterface({
  uri: '/graphql',
  opts: {
    credentials: 'same-origin',
  },
});

// Apollo Client
const client = new ApolloClient({
  dataIdFromObject: o => o.id,
  networkInterface,
});

class Client extends Component {
  render() {
    return (
      <ApolloProvider client={ client }>
        <Routes />
      </ApolloProvider>
    );
  }
}

export default Client;

import React, { Component } from 'react';
import ErrorMessage from './ErrorMessage';

class AuthForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  onSubmit(e) {
    e.preventDefault();
    const { email, password } = this.state;
    this.props.onSubmit({ email, password });
  }

  render() {
    return (
      <div className="row">
        <form
          className="col s12 m8 l6"
          onSubmit={ this.onSubmit.bind(this) }
        >
          <div className="input-field">
            <input
              placeholder="Email"
              type="text"
              value={ this.state.email }
              onChange={ e => this.setState({ email: e.target.value })}
            />
          </div>
          <div className="input-field">
            <input
              placeholder="Password"
              type="password"
              value={ this.state.password }
              onChange={ e => this.setState({ password: e.target.value })}
            />
          </div>
          { this.props.errors.map((e, key) => <ErrorMessage error={ e } key={ key } />) }
          <button className="btn">Submit</button>
        </form>
      </div>
    );
  }
}

export default AuthForm;

import React from 'react';

const ErrorMessage = props => (
  <div className="error">
    { props.error }
  </div>
);

export default ErrorMessage;

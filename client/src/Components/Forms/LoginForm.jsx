import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import AuthForm from './AuthForm';
import mutation from '../../queries/login';
import query from '../../queries/currentUser';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.loginSubmit = this.loginSubmit.bind(this);
    this.state = {
      errors: [],
    };
  }

  loginSubmit({ email, password }) {
    this.setState({
      errors: [],
    });
    this.props.mutate({
      variables: { email, password },
      refetchQueries: [{ query }],
    })
    .then(res => this.props.redirect())
    .catch(res =>
      this.setState({ errors: res.graphQLErrors.map(err => err.message) }));
  }

  render() {
    return (
      <div>
        <h3>Login</h3>
        <AuthForm
          errors={ this.state.errors }
          onSubmit={ this.loginSubmit }
        />
      </div>
    );
  }
}

export default graphql(mutation)(LoginForm);

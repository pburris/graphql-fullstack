import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { Link } from 'react-router';
import query from '../../queries/currentUser';
import mutation from '../../queries/logout';

class Header extends Component {
  constructor(props) {
    super(props);
    this.onLogoutClick = this.onLogoutClick.bind(this);
  }

  onLogoutClick() {
    this.props.mutate({
      refetchQueries: [{ query }],
    });
  }

  renderButtons() {
    const { user, loading } = this.props.data;
    if (loading) return <div />;
    if (user) return (
      <li>
        <a onClick={ this.onLogoutClick }>Logout</a>
      </li>
    );
    return (
      <div>
        <li>
          <Link to="/signup">Signup</Link>
        </li>
        <li>
          <Link to="/login">Login</Link>
        </li>
      </div>
    );
  }

  render() {
    return (
      <nav>
        <div className="nav-wrapper">
          <div className="container">
            <Link to="/" className="brand-logo left">Home</Link>
            <ul className="right">
              { this.renderButtons() }
            </ul>
          </div>
				</div>
      </nav>
    );
  }
}

export default graphql(mutation)(
  graphql(query)(Header)
);

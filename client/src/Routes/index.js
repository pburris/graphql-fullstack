import React from 'react';
import { Route, Router, IndexRoute, hashHistory } from 'react-router';

// Routes

// Layouts
import App from './layouts/App';
// Views
import Home from './views/Home';
import Login from './views/Login';
import Signup from './views/Signup';

const Routes = props => (
  <Router history={ hashHistory }>
    <Route path="/" component={ App } >
      <IndexRoute component={ Home } />
      <Route path="login" component={ Login } />
      <Route path="signup" component={ Signup } />
    </Route>
  </Router>
);

export default Routes;

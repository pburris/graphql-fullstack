import React, { Component } from 'react';
import { Header } from '../../Components/Header';

const App = props => (
  <div>
    <Header />
    <main className="container">
      { props.children }
    </main>
  </div>
);

export default App;

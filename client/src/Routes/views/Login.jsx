import React from 'react';
import { LoginForm } from '../../Components/Forms';

const Login = props => (
  <main>
    <LoginForm redirect={ () => props.router.push('/') } />
  </main>
);

export default Login;

import React from 'react';
import { SignupForm } from '../../Components/Forms';

const Signup = props => (
  <main>
    <SignupForm redirect={ () => props.router.push('/') } />
  </main>
);

export default Signup;

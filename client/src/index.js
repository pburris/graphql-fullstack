import React from 'react';
import { render } from 'react-dom';
import Client from './Client';

// Styles
require('./styles/main.css');

// Takes in the client and appends it to the DOM
const app = document.getElementById('app');
render(<Client />, app);

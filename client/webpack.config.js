const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const PATHS = {
  app: path.join(__dirname, 'src'),
  build: path.join(__dirname, '../server/public'),
};

const config = {
  entry: {
    app: PATHS.app,
  },
  output: {
    path: PATHS.build,
    filename: '[name].js',
  },
	resolve: {
		extensions: ['.js', '.jsx']
	},
  devServer: {
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": "true",
      "Access-Control-Allow-Headers": "Content-Type, Authorization, x-id, Content-Length, X-Requested-With",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, OPTIONS",
    },
  },
  module: {
    rules: [

      // Babel
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        include: PATHS.app
      },

			// Styles
			{
				test: /\.css$/,
				loader: 'style-loader!css-loader',
			},

    ],
  },
	plugins: [
		new HtmlWebpackPlugin({
      title: 'GrahpQL BoilerPlate',
      template: 'templates/main.html',
    }),
    new CleanWebpackPlugin([PATHS.build], {
      root: './',
    })
	],
};

module.exports = config;

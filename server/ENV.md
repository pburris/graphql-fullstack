# Environment variables needed

1. `PORT` - server port
2. `MONGO_URL` - url to mongodb server
3. `SESSION_SECRET` - secret for JWT session

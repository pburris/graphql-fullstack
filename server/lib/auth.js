import mongoose from 'mongoose';
import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';

// User Model
const User = mongoose.model('user');

/**
 * Serialize User
 *
 * provide a way to identify users by adding their ID to the session
 */
passport.serializeUser((user, done) => {
  done(null, user.id);
});

/**
 * Deserialize User
 *
 * Provide a way to get the user information from just the serialized ID
 */
passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user);
  });
});


/**
 * Local Strategy
 *
 * 1. Find User in MongoDB
 * 2. Compare incoming password
 * 3. Call callback
 */
passport.use(new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
  User.findOne({ email: email.toLowerCase() }, (err, user) => {
    if (err) { return done(err); }
    if (!user) { return done(null, false, 'Invalid Credentials'); }
    user.comparePassword(password, (err, isMatch) => {
      if (err) { return done(err); }
      if (isMatch) {
        return done(null, user);
      }
      return done(null, false, 'Invalid credentials.');
    });
  });
}));

/**
 * Signup
 *
 * 1. Check if a user already exists with incoming email to avoid dupe accounts
 * 2. If it doesn't exist we save the account
 * 3. Login the new user in
 * 4. Return a promise to allow graphQL to get involved
 */

const signup = ({ email, password, req }) => {
  const user = new User({ email, password });
  if (!email || !password) { throw new Error('You must provide an email and password.'); }

  return User.findOne({ email })
    .then(existingUser => {
      if (existingUser) { throw new Error('Email in use'); }
      return user.save();
    })
    .then(user => {
      return new Promise((resolve, reject) => {
        req.logIn(user, (err) => {
          if (err) { reject(err); }
          resolve(user);
        });
      });
    });
};

/**
 * Login
 *
 * Using the local strategy above using the authenticate method. We return a Promise
 * allowing us to stay compatible with GraphQL
 */
const login = ({ email, password, req }) => {
  return new Promise((resolve, reject) => {
    passport.authenticate('local', (err, user) => {
      if (!user) reject('Invalid credentials.');
      req.login(user, () => resolve(user));
    })({ body: { email, password } });
  });
};

export {
  signup,
  login,
};

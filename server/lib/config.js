/**
 * General Configuration Object
 *
 * This file is for importing process.env variables as well as general configurations
 * for the libraries that we will be using.
 */
const { env } = process;

/**
 * 1. Server (server)
 *    a. port
 *    b. secret
 * 2. MongoDB (mongo)
 *    a. url
 */


const config = {
  server: {
    port: env.PORT || 3000,
    secret: env.SESSION_SECRET || 'Operation secret sauce',
  },
  mongo: {
    url: env.MONGO_URL || "http://localhost:27017/dev",
  },
};


export default config;

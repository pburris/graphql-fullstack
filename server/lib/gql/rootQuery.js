import graphql, {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLID,
} from 'graphql';
import UserType from './userType';

const RootQueryType = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    user: {
      type: UserType,
      args: {
        id: { type: GraphQLID },
      },
      resolve(parentValue, args, req) {
        return req.user;
      },
    },
  },
});

export default RootQueryType;

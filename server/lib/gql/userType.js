import {
  GraphQLString,
  GraphQLInt,
  GraphQLObjectType,
  GraphQLID,
} from 'graphql';

const UserType = new GraphQLObjectType({
  name: 'User',
  fields: () => ({
    id: { type: GraphQLID },
    email: { type: GraphQLString },
  }),
});

export default UserType;

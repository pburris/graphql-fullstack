/**
 * Database
 *
 * Using mongodb via mongoose we are initializing the database connection and setting
 * some behavior about it.
 * Afterwards export this mongoose object for use throughout the app.
 *
 * NOTE: Only mutate the mongoose object here.
 */
import mongoose from 'mongoose';
import config from './config.js';
import UserSchema from '../models/user';

// Models
mongoose.model('user', UserSchema);

// Connect
mongoose.Promise = global.Promise;
mongoose.connect(config.mongo.url);
mongoose.connection
    .once('open', () => console.log('Connected to MongoLab instance.'))
    .on('error', error => console.log('Error connecting to MongoLab:', error));

export default mongoose;

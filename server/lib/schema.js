import graphql, { GraphQLSchema } from 'graphql';
import RootQueryType from './gql/rootQuery';
import mutation from './gql/mutations';

const schema = new GraphQLSchema({
  query: RootQueryType,
  mutation,
});

export default schema;

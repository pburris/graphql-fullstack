import bcrypt from 'bcrypt-nodejs';
import crypto from 'crypto';
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/**
 * User Schema
 *
 * email     String email/username
 * password  String password -- NOT stored as plain text
 */
const UserSchema = new Schema({
  email: String,
  password: String,
});

/**
 * User Pre-Save
 *
 * The password is not stored as plain text. Before saving the user model,
 * we salt and hash the password. This is used with ::comparePassword to
 * authenticate users.
 */
UserSchema.pre('save', function save(next) {
  const user = this;
  if (!user.isModified('password')) { return next(); }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);
    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  });
});

/**
 * Compare Password
 *
 * This method allows us to compare submitted passwords to the salted + hashed password
 * that is stored. Bcrypt's built in compare function allows us to do this. Passwords
 * are never compared as plain text because the encryption process is one way.
 */
UserSchema.methods.comparePassword = function comparePassword(incPass, cb) {
  bcrypt.compare(incPass, this.password, (err, isMatch) => {
    cb(err, isMatch);
  });
};

export default UserSchema;

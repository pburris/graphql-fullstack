import express from 'express';
import expressGraphQL from 'express-graphql';
import session from 'express-session';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import passport from 'passport';
import { config, mongoose, schema } from './lib';
import * as auth from './lib/auth';


// Define constants
const app = express();
const MongoStore = require('connect-mongo')(session);
app.set('port', config.server.port);
const User = mongoose.model('user');


/**
 * Middleware
 *
 * in the form of app.use()
 * 1. Morgan for logging
 * 2. Body and Cookie parsers
 * 3. Express Session
 * 4. Passport auth
 * 5. GraphQL
 * 6. Headers
 */

// Morgan
app.use(morgan('dev'));
// Parsers
app.use(bodyParser.urlencoded({
  extended: true,
}));
app.use(bodyParser.json());
app.use(cookieParser());
// Session
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: config.server.secret,
  store: new MongoStore({
    url: config.mongo.url,
    autoReconnect: true
  }),
}));
// Passport
app.use(passport.initialize());
app.use(passport.session());
// GraphQL
app.use('/graphql', expressGraphQL({
  schema,
  graphiql: true
}));
// Headers
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


// Routes
app.use('/', express.static('public'));

app.get('/new', (req, res) => {
  const newUser = new User({
    email: 'nchupacabra@gmail.com',
    password: 'password',
  });
  newUser.save((err) => {
    if (err) res.end(JSON.stringify(err));
    res.end(JSON.stringify(newUser));
  });
});


export default app;
